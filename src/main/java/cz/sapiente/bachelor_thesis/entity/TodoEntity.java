package cz.sapiente.bachelor_thesis.entity;

import java.time.OffsetDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(name = "todos")
@NoArgsConstructor
public class TodoEntity {

    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    private ProjectEntity project;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Boolean done;

    @Getter
    @Setter
    private OffsetDateTime dueDate;

    @Getter
    @Setter
    private String priority;
}
