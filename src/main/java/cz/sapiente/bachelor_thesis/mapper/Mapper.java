package cz.sapiente.bachelor_thesis.mapper;

import java.util.List;

public interface Mapper {
    <D> D map(Object source, Class<D> destinationType);
    <D> List<D> map(List source, Class<D> destinationType);
}
