package cz.sapiente.bachelor_thesis.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

import cz.sapiente.bachelor_thesis.entity.ProjectEntity;

public interface ProjectRepository extends CrudRepository<ProjectEntity, Long> {
    List<ProjectEntity> findProjectsByUserId(Long userId);
}
