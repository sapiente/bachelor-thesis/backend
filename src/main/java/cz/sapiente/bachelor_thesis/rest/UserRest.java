package cz.sapiente.bachelor_thesis.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.sapiente.bachelor_thesis.dto.request.RegisterRequest;
import cz.sapiente.bachelor_thesis.service.UserService;

@RestController
@RequestMapping("/users")
public class UserRest {

    private final UserService userService;

    public UserRest(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public void create(@RequestBody final RegisterRequest request) {
        this.userService.register(request);
    }
}
