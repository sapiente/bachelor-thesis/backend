package cz.sapiente.bachelor_thesis.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import cz.sapiente.bachelor_thesis.dto.request.LoginRequest;
import cz.sapiente.bachelor_thesis.entity.UserEntity;

@Service
public class AuthService {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Value("${jwt_secret}")
    private String secret;
    @Value("${jwt_expiration}")
    private int expiration;

    public AuthService(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public UserEntity getLoggedUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return this.userService.findUser((String) authentication.getPrincipal());
    }


    public String login(final LoginRequest request) throws UnsupportedEncodingException {
        final UserEntity user = this.userService.findUser(request.getUsername());
        if (user == null || !this.passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new BadCredentialsException(null);
        }

        final Calendar expiration = Calendar.getInstance();
        expiration.add(Calendar.MINUTE, this.expiration);

        return JWT.create()
                .withIssuer("ibp")
                .withClaim("username", user.getUsername())
                .withClaim("id", user.getId())
                .withExpiresAt(expiration.getTime())
                .sign(getAlgorithm());
    }

    public UserEntity authenticate(final String token) throws UnsupportedEncodingException {
        final JWTVerifier verifier = JWT.require(getAlgorithm())
                .withIssuer("ibp")
                .build();

        final DecodedJWT jwt = verifier.verify(token);

        return this.userService.findUser(jwt.getClaim("username").asString());
    }

    private Algorithm getAlgorithm() throws UnsupportedEncodingException {
        return Algorithm.HMAC512(this.secret);
    }
}
