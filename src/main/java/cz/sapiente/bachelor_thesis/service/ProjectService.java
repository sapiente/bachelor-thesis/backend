package cz.sapiente.bachelor_thesis.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

import javax.transaction.Transactional;

import cz.sapiente.bachelor_thesis.dto.request.CreateProjectRequest;
import cz.sapiente.bachelor_thesis.dto.request.UpdateProjectRequest;
import cz.sapiente.bachelor_thesis.entity.ProjectEntity;
import cz.sapiente.bachelor_thesis.entity.UserEntity;
import cz.sapiente.bachelor_thesis.repository.ProjectRepository;
import cz.sapiente.bachelor_thesis.repository.TodoRepository;

@Service
@Transactional
public class ProjectService {
    private final ProjectRepository projectRepository;
    private final TodoRepository todoRepository;
    private final AuthService authService;

    public ProjectService(final ProjectRepository projectRepository, final TodoRepository todoRepository, final AuthService authService) {
        this.projectRepository = projectRepository;
        this.todoRepository = todoRepository;
        this.authService = authService;
    }

    public List<ProjectEntity> findLoggedUserProjects() {
        final UserEntity loggedUser = this.authService.getLoggedUser();

        return this.projectRepository.findProjectsByUserId(loggedUser.getId());
    }

    public ProjectEntity createProjectForLoggedUser(final CreateProjectRequest request) {
        final UserEntity loggedUser = this.authService.getLoggedUser();
        final ProjectEntity entity = new ProjectEntity(loggedUser, request.getName());

        return this.projectRepository.save(entity);
    }

    public void deleteLoggedUserProject(final long projectId) {
        final ProjectEntity entity = this.projectRepository.findById(projectId).orElseThrow(() -> new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY));
        final UserEntity loggedUser = this.authService.getLoggedUser();

        if (! entity.getUser().equals(loggedUser)) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
        }

        this.todoRepository.deleteTodosByProjectId(projectId);
        this.projectRepository.delete(entity);
    }

    public ProjectEntity updateLoggedUserProject(final long projectId, final UpdateProjectRequest request) {
        final ProjectEntity entity = this.projectRepository.findById(projectId).orElseThrow(() -> new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY));
        final UserEntity loggedUser = this.authService.getLoggedUser();

        if (! entity.getUser().equals(loggedUser)) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
        }

        entity.setName(request.getName());

        return this.projectRepository.save(entity);
    }
}
