package cz.sapiente.bachelor_thesis.service;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import cz.sapiente.bachelor_thesis.dto.request.RegisterRequest;
import cz.sapiente.bachelor_thesis.entity.UserEntity;
import cz.sapiente.bachelor_thesis.repository.UserRepository;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(final UserRepository userRepository, final PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void register(final RegisterRequest request) {
        if (this.userRepository.findByUsername(request.getUsername()) != null) {
           throw new HttpClientErrorException(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        final String encryptedPassword = passwordEncoder.encode(request.getPassword());
        final UserEntity user = new UserEntity(request.getUsername(), encryptedPassword);

        this.userRepository.save(user);
    }

    public UserEntity findUser(final String username) {
        return this.userRepository.findByUsername(username);
    }
}
